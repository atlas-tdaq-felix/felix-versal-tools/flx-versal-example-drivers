#ifndef FLXNET_H
#define FLXNET_H

#include <linux/etherdevice.h>

struct tx_queue {
	int head;
	int tail;
	struct tx_packet *packets;
};

struct flxnet_peer {
	struct list_head peer_entry;
	void __iomem *regs;
	unsigned int reg_send;
	unsigned int reg_recv;
	unsigned int reg_status;
	unsigned char mac_addr[ETH_ALEN];
	struct net_device *flxnet;
	struct tx_queue tx_queue;
};

// determine role based on cpu
#ifdef __x86_64__
#define IS_ROLE_HOST
#else
#define IS_ROLE_TARGET
#endif

#define MIN_MMAP_SIZE 0x20

// fifo operators
#define HOST_ADDR_SEND 0x00
#define HOST_ADDR_RECV 0x10
#define HOST_ADDR_STATUS 0x20

#define HOST_MAGIC_REG 0x60

#define TARGET_ADDR_SEND 0x0C
#define TARGET_ADDR_RECV 0x10
#define TARGET_ADDR_STATUS 0x14

#define TARGET_MAGIC_REG 0x18

#define SET_CARRIER 7
#define STATUS_CARRIER 6
#define SEND_IS_EOP 5
#define RECV_IS_EOP 4
#define HAS_ROOM_FOR_BLOCK 3
#define HAS_ROOM_FOR_WORD 2
#define BLOCK_AVAILABLE 1
#define WORD_AVAILABLE 0

#define FLX_DEV_MAGIC	0xCECABABAUL

#endif