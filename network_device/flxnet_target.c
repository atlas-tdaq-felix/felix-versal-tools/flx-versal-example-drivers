/*

	This is the network driver for FLX-182 side
	to create a network interface over PCIe

	Device tree entry example:

	&amba {
        flxnet_instance: flxnet@20100030000 {
            #address-cells = <0x02>;
            #size-cells = <0x02>;
            reg = <0x201 0x00030000 0x00 0x100>;        
            compatible = "atlas_tdaq_felix,VERSAL_virtual_network_device_v1.0";
            status = "okay";
        };
    };

*/

#include <linux/kernel.h>
#include <linux/moduleparam.h>
#include <linux/stat.h>
#include <linux/platform_device.h>
#include <linux/mm.h>
#include <linux/io.h>
#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/vmalloc.h>
#include <linux/module.h>
#include <linux/version.h>

#include <linux/of_address.h>
#include <linux/of_device.h>
#include <linux/of_platform.h>

#include "flxnet/flxnet.h"

#define DRV_MODULE_NAME	"flxnet_target"

extern struct flxnet_peer * flxnet_add_peer(void* __iomem base_address);
extern void flxnet_remove_peer(struct flxnet_peer *peer);
struct flxnet_peer *peer;

static int flx_net_probe(struct platform_device *pdev) {
	void * __iomem base;
	struct resource *mem;
	
	pr_info("flxnet_target: probing is starting\n");

	mem = platform_get_resource(pdev, IORESOURCE_MEM, 0);
	if (!mem) {
		dev_err(&pdev->dev, "flxnet_target: failed to get memory resource\n");
		return -ENODEV;
	}

	base = devm_ioremap_resource(&pdev->dev, mem);
	pr_info("flxnet_target: target base address: %X\n", (uint32_t)base);

	if (IS_ERR(base)) {
		dev_err(&pdev->dev, "flxnet_target: failed to ioremap memory resource\n");
		return -ENODEV;
	}

	// add this device to the network
	dev_info(&pdev->dev, "flxnet_target: registering a peer with flxnet_dev\n");
	peer = flxnet_add_peer(base);
	if (IS_ERR(peer)) {
		dev_err(&pdev->dev, "flxnet_target: failed to insert flxnet peer\n");
		return -ENODEV;
	}

	platform_set_drvdata(pdev, peer);
	return 0;
}


static int flx_net_remove(struct platform_device *pdev) {
	flxnet_remove_peer(peer);
	return 0;
}


static const struct of_device_id flx_serial_of_match[] = {
	{ .compatible = "atlas_tdaq_felix,VERSAL_virtual_network_device_v1.0", .data = NULL },
	{ /* end of table */ }
};
MODULE_DEVICE_TABLE(of, flx_serial_of_match);

static struct platform_driver flx_net_driver = {
	.driver	= {
		.name			= DRV_MODULE_NAME,
		.owner 			= THIS_MODULE,
		.of_match_table = flx_serial_of_match,
	},
	.probe	= flx_net_probe,
	.remove = flx_net_remove,
};

static int __init flxnet_target_init(void) {
	pr_info("flxnet_target: initializing %s module\n", DRV_MODULE_NAME);
	return platform_driver_register(&flx_net_driver);
}


static void __exit flxnet_target_exit(void) {
	pr_info("flxnet_target: removing %s module\n", DRV_MODULE_NAME);
	platform_driver_unregister(&flx_net_driver);
}

module_init(flxnet_target_init);
module_exit(flxnet_target_exit);

MODULE_AUTHOR("Elena Zhivun <elena.zhivun@cern.ch>");
MODULE_AUTHOR("Laura Rientsma <l.rientsma@nikhef.nl>");
MODULE_DESCRIPTION("Driver on Versal card for establishing virtual network over PCIe, registers with flxnet_dev");
MODULE_LICENSE("GPL");

