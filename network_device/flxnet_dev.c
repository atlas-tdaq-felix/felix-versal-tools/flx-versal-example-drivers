/*

	This is the main driver for establishing a network 
	between multiple FLX-182 card and the host PC over PCIe
	This network interface device driver runs on both sides
	
*/

#include <linux/kernel.h>
#include <linux/vmalloc.h>
#include <linux/errno.h>
#include <linux/io.h>
#include <linux/device.h>
#include <linux/timer.h>
#include <linux/types.h>
#include <linux/ethtool.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/kthread.h>
#include <linux/module.h>
#include <linux/version.h>
#include <linux/circ_buf.h>

#include "flxnet/flxnet.h"

#define TX_TIMEOUT_MS 6
#define RX_TIMEOUT_MS 3
#define POLL_PERIOD_MS 1
#define SLEEP_PERIOD_MS 1000

#define DRV_NAME "flxnet_dev"

struct mutex flxnet_mutex;
u32 message_level;

typedef u32 t_fifo_word;

#define MAX_PACKET_LEN 400 // measured in FIFO words
#define BYTES_IN_WORD 4
#define MAX_DATA_LEN MAX_PACKET_LEN * BYTES_IN_WORD
#define MAX_RETRY_ATTEMPTS 5

struct tx_packet {
	int	num_words;
	int attempts;
	char dest_mac[ETH_ALEN];
	char data[MAX_DATA_LEN];
};

int tx_queue_size = 256;
DEFINE_SPINLOCK(tx_queue_spinlock);

struct list_head peer_list;

struct mutex peer_mutex;
atomic_t peer_count;

wait_queue_head_t recv_queue;
struct timer_list recv_timer;
struct task_struct *recv_task;


#define FLXNET_SOP 0xCACE0000
#define FLXNET_SOP_MASK 0xFFFF0000
#define FLXNET_LEN_MASK 0x0000FFFF
#define FLXNET_EOP 0xFA18CECA

#define RX_SUCCESS 0
#define RX_LOW_BUFFER 1
#define RX_INVALID_ARG 2
#define RX_TIMEOUT 3
#define RX_INVALID_EOP 4
#define RX_NO_MEMORY 5
#define RX_TRUNCATED 6

#define TX_SUCCESS 0
#define TX_TRY_BROADCAST 1
#define TX_BUSY 2
#define TX_ERROR 3

// Read/Write access to the MMAPped registers
#if defined(CONFIG_ARCH_ZYNQ) || defined(CONFIG_X86)
# define register_read(offset)		readl(offset)
# define register_write(offset, val)	writel(val, offset)
#else
# define register_read(offset)		__raw_readl(offset)
# define register_write(offset, val)	__raw_writel(val, offset)
#endif

static void tx_all_packets(struct flxnet_peer *peer);

static inline bool check_flag(void* __iomem reg, int bit_id) {
	return ((1 << bit_id) & register_read(reg)) != 0;
}

static inline void set_flag(void* __iomem reg, int bit_id) {
	uint32_t temp_reg = register_read(reg);
	temp_reg |= (1 << bit_id);
	register_write(reg, temp_reg);
	//pr_info("flxnet_dev: flag set\n");
}

static inline void clear_flag(void* __iomem reg, int bit_id) {
	uint32_t temp_reg = register_read(reg);
	temp_reg &= ((1 << bit_id)^0xFFFFFFFF);
	register_write(reg, temp_reg);
	//pr_info("flxnet_dev: flag cleared\n");
}

/* static inline void print_status_reg(struct flxnet_peer *peer) {
	uint32_t temp_reg = register_read(peer->regs + peer->reg_status);
	pr_info("flxnet_dev: SEND_EOP: %d\nRECV_EOP: %d\nROOM_FOR_BLOCK: %d\nROOM_FOR_WORD: %d\nBLOCK_AVAILABLE: %d\nWORD_AVAILABLE: %d\n"
          (temp_reg & (1<<SEND_IS_EOP) >0),
          (temp_reg & (1<<RECV_IS_EOP) >0),
          (temp_reg & (1<<HAS_ROOM_FOR_BLOCK) >0),
          (temp_reg & (1<<HAS_ROOM_FOR_WORD) >0),
          (temp_reg & (1<<BLOCK_AVAILABLE) >0),
          (temp_reg & (1<<WORD_AVAILABLE) >0)
	);
} */

static inline bool is_word_available(struct flxnet_peer *peer) {
	return check_flag(peer->regs + peer->reg_status, WORD_AVAILABLE);
}

static inline bool is_block_available(struct flxnet_peer *peer) {
	return check_flag(peer->regs + peer->reg_status, BLOCK_AVAILABLE);
}

static inline bool can_accept_word(struct flxnet_peer *peer) {
	return check_flag(peer->regs + peer->reg_status, HAS_ROOM_FOR_WORD);
}

static inline bool can_accept_block(struct flxnet_peer *peer) {
	return check_flag(peer->regs + peer->reg_status, HAS_ROOM_FOR_BLOCK);
}

static inline bool recv_eop(struct flxnet_peer *peer) {
	return check_flag(peer->regs + peer->reg_status, RECV_IS_EOP);
}

// send a single word to the device
static inline void send_word(struct flxnet_peer* peer, u32 data) {
	register_write(peer->regs + peer->reg_send, data);
}

// receive a single word
static inline t_fifo_word recv_word(struct flxnet_peer* peer) {
	return (t_fifo_word)register_read(peer->regs + peer->reg_recv);
}

static inline void set_carrier(struct flxnet_peer *peer, bool value) {
	if (value) {
		set_flag(peer->regs + peer->reg_status, SET_CARRIER);
		if (message_level & NETIF_MSG_IFUP)
			pr_info("flxnet_dev: setting carrier to %d\n", value);
	}
	else {
		clear_flag(peer->regs + peer->reg_status, SET_CARRIER);
		if (message_level & NETIF_MSG_IFDOWN)
			pr_info("flxnet_dev: setting carrier to %d\n", value);
	}
}

static inline bool status_carrier(struct flxnet_peer *peer) {
	bool value = check_flag(peer->regs + peer->reg_status, STATUS_CARRIER);
	return value;
}

// send end of packet
static inline void send_eop(struct flxnet_peer *peer) {
	set_flag(peer->regs + peer->reg_status, SEND_IS_EOP);
	send_word(peer, FLXNET_EOP);
	if (message_level & NETIF_MSG_TX_DONE)
		pr_info("flxnet_dev: end of packet sent\n");
	clear_flag(peer->regs + peer->reg_status, SEND_IS_EOP);
}


// verify that the device magic number is correct
inline bool is_magic_correct(struct flxnet_peer* peer) {
	#ifdef IS_ROLE_TARGET
		return (register_read(peer->regs + TARGET_MAGIC_REG) == FLX_DEV_MAGIC);
	#else
		return (register_read(peer->regs + HOST_MAGIC_REG) == FLX_DEV_MAGIC);
	#endif
}


struct flxnet_peer *find_peer_for_netdev(struct net_device *dev) {
	struct flxnet_peer *peer;
	struct flxnet_peer *tmp;
	struct flxnet_peer *return_peer = NULL;

	if (message_level & NETIF_MSG_LINK)
		pr_info("flxnet_dev: finding peer for net device\n");

	list_for_each_entry_safe(peer, tmp, &peer_list, peer_entry) {
		if (peer->flxnet == dev) {
			return_peer = peer;
		}
	}
	return return_peer;
}

// removes the peer from the network
void flxnet_remove_peer(struct flxnet_peer *peer) {
	int rv;

	if (message_level & NETIF_MSG_LINK)
		pr_info("flxnet_dev: removing peer %p from the network\n", peer);

	mutex_lock(&peer_mutex);
    set_carrier(peer, false);
	unregister_netdev(peer->flxnet);
	free_netdev(peer->flxnet);
	kfree(peer->tx_queue.packets);

	rv = atomic_dec_return(&peer_count);
	if (rv < 0) {
		pr_err("flxnet_dev: BUG in %s: peer_count = %d, less than 0\n",
			__FUNCTION__, rv);
		atomic_set(&peer_count, 0);
	}

	list_del(&peer->peer_entry);
	kfree(peer);
	mutex_unlock(&peer_mutex);

	if (list_empty(&peer_list)) {
		// disable RX side when all peers are removed
		if (message_level & NETIF_MSG_IFDOWN)
			pr_info("flxnet_dev: stop RX thread\n");	
		wake_up_interruptible(&recv_queue);
		kthread_stop(recv_task);
		del_timer_sync(&recv_timer);
	}
}

static bool tx_queue_is_full(struct flxnet_peer *peer) {
	bool rv;
	unsigned long flags;
	spin_lock_irqsave(&tx_queue_spinlock, flags);
	rv = CIRC_SPACE(peer->tx_queue.head, peer->tx_queue.tail, tx_queue_size) == 0;
	spin_unlock_irqrestore(&tx_queue_spinlock, flags);
	return rv;
} 


static bool tx_queue_is_empty(struct flxnet_peer *peer) {
	bool rv;
	unsigned long flags;
	spin_lock_irqsave(&tx_queue_spinlock, flags);
	rv = CIRC_CNT(peer->tx_queue.head, peer->tx_queue.tail, tx_queue_size) == 0;
	spin_unlock_irqrestore(&tx_queue_spinlock, flags);
	return rv;
}

	
static int tx_queue_try_push(struct sk_buff *skb, struct net_device *dev) {
	struct tx_packet *packet;
	struct ethhdr *mac_hdr;
	unsigned long flags;
	struct flxnet_peer *peer; 

	peer = find_peer_for_netdev(dev);

	if (tx_queue_is_full(peer)) {
		if (message_level & NETIF_MSG_TX_ERR)
			pr_info("flxnet_dev: trying to add a packet to TX queue, but it is full\n");
		return TX_BUSY;
	}

	if (skb->len > MAX_DATA_LEN) {
		pr_err("flxnet_dev: (BUG) TX packet payload is too large "
			"(%d bytes, maximum is %d)\n", skb->len, MAX_DATA_LEN);
		return TX_ERROR;
	}

	mac_hdr = eth_hdr(skb);

	spin_lock_irqsave(&tx_queue_spinlock, flags);
	packet = &peer->tx_queue.packets[peer->tx_queue.head];
	packet->num_words = skb->len / BYTES_IN_WORD;
	memcpy(packet->data, skb->data, skb->len);
	if (skb->len % BYTES_IN_WORD) {
		packet->num_words += 1;
		memset(packet->data + skb->len, 0, BYTES_IN_WORD - (skb->len % BYTES_IN_WORD));
	}
	memcpy(packet->dest_mac, mac_hdr->h_dest, ETH_ALEN);
	packet->attempts = 0;
	peer->tx_queue.head = (peer->tx_queue.head + 1) & (tx_queue_size - 1);
	spin_unlock_irqrestore(&tx_queue_spinlock, flags);

	return TX_SUCCESS;
}


static int tx_queue_pop(struct flxnet_peer *peer) {
	unsigned long flags;

	if (tx_queue_is_empty(peer)) {
		pr_warn("flxnet_dev: trying to remove a packet from TX queue, but it is empty\n");
		return -1;
	}

	spin_lock_irqsave(&tx_queue_spinlock, flags);
	peer->tx_queue.tail = (peer->tx_queue.tail + 1) & (tx_queue_size - 1);
	spin_unlock_irqrestore(&tx_queue_spinlock, flags);
	return 0;
}


void tx_queue_reset(struct net_device *dev) {
	struct flxnet_peer *peer;
	unsigned long flags;

	peer = find_peer_for_netdev(dev);
	dev->stats.tx_dropped += CIRC_CNT(peer->tx_queue.head, peer->tx_queue.tail, tx_queue_size);
	spin_lock_irqsave(&tx_queue_spinlock, flags);
	peer->tx_queue.head = 0;
	peer->tx_queue.tail = 0;
	spin_unlock_irqrestore(&tx_queue_spinlock, flags);	
}


static struct tx_packet * tx_queue_get_next(struct flxnet_peer *peer) {
	struct tx_packet * result;
	unsigned long flags;

	if (tx_queue_is_empty(peer))
		return NULL;

	spin_lock_irqsave(&tx_queue_spinlock, flags);
	result = &peer->tx_queue.packets[peer->tx_queue.tail];
	spin_unlock_irqrestore(&tx_queue_spinlock, flags);
	return result;
}


// waits for EOP, returns 0 if it is received correctly, -1 otherwise
bool is_valid_eop(struct flxnet_peer *peer, unsigned long timeout) {
	t_fifo_word eop;

	eop = 0;
	while (true) {
		if (is_word_available(peer)) {
			eop = recv_word(peer);
			break;
		}
		if (jiffies > timeout) {
			break;
		}
	}

	if (jiffies > timeout || eop != FLXNET_EOP) {
		return false;
	}

	return true;
}

// check the mac address of the received packet, update the peer mac if valid
void update_mac(struct flxnet_peer *peer, struct sk_buff *skb) {
	struct ethhdr *mac_hdr;

	mac_hdr = eth_hdr(skb);
	if (!is_valid_ether_addr(mac_hdr->h_source)) {
		pr_info("flxnet_dev: mac address of received packet is invalid\n");
		return;
	}

	if (memcmp(peer->mac_addr, mac_hdr->h_source, ETH_ALEN) != 0) {
		if (message_level & NETIF_MSG_DRV) {
			pr_info("flxnet_dev: updating peer mac address: was %pM, new %pM\n",
				peer->mac_addr, mac_hdr->h_source);
		}
		memcpy(peer->mac_addr, mac_hdr->h_source, ETH_ALEN);
	}	
}


// there is no memory to create skb, so deque the packet and drop it
int drop_rx_packet(struct flxnet_peer *peer, int length_words) {
	int num_words;
	unsigned long timeout;


	if (length_words == 0) {
		if (message_level & NETIF_MSG_DRV)
			pr_err("flxnet_dev: attempting to drop 0-length packet\n");
		peer->flxnet->stats.rx_frame_errors += 1;
		return RX_INVALID_ARG;
	}

	timeout = jiffies + msecs_to_jiffies(RX_TIMEOUT_MS);

	num_words = 0;
	while (true) {
		while (is_word_available(peer)) {
			recv_word(peer);
			num_words += 1;
			if (num_words == length_words) {
				goto packet_rx_done;
			}
		}

		if (jiffies > timeout) {
			goto packet_rx_done;
		}
	}

packet_rx_done:

	peer->flxnet->stats.rx_bytes += num_words * BYTES_IN_WORD;

	if (jiffies > timeout) {
		return RX_TIMEOUT;	
	}

	if (num_words != length_words) {
		return RX_TRUNCATED;
	}

	if (!is_valid_eop(peer, timeout)) {
		peer->flxnet->stats.rx_frame_errors += 1;
		return RX_INVALID_EOP;
	}
	
	return RX_NO_MEMORY;
}


int recv_packet(struct flxnet_peer *peer, int length_words) {
	struct sk_buff *skb;	
	int num_words;
	t_fifo_word rx_word;
	unsigned long timeout;
	bool low_buffer;	

	if (length_words == 0 || length_words > MAX_PACKET_LEN) {
		if (message_level & NETIF_MSG_DRV)
			pr_err("flxnet_dev: invalid packet length (%d words)\n",
				length_words);
		peer->flxnet->stats.rx_frame_errors += 1;
		return RX_INVALID_ARG;
	}	

	skb = netdev_alloc_skb(peer->flxnet, length_words * BYTES_IN_WORD);
	if (!skb) {
		peer->flxnet->stats.rx_dropped++;
		if (message_level & NETIF_MSG_RX_ERR)
			pr_err("flxnet_dev: dropped RX packet, low memory\n");
		drop_rx_packet(peer, length_words);
		return RX_NO_MEMORY;
	}

	// socket buffer is available, receive the packet
	timeout = jiffies + msecs_to_jiffies(RX_TIMEOUT_MS);

	num_words = 0;
	low_buffer = false;
	if (message_level & NETIF_MSG_PKTDATA)
		pr_info("flxnet_dev: received packet data:\n");

	while (true) {
		while (is_word_available(peer)) {
			rx_word = recv_word(peer);
			memcpy(skb_put(skb, BYTES_IN_WORD), &rx_word, BYTES_IN_WORD);
			num_words += 1;
			if (message_level & NETIF_MSG_PKTDATA)
				pr_info("%08X\n", rx_word);
			if (num_words == length_words)
				goto packet_rx_done;
		}

		low_buffer = true;

		if (jiffies > timeout) {
			goto packet_rx_done;
		}
	}

packet_rx_done:

	peer->flxnet->stats.rx_bytes += num_words * BYTES_IN_WORD;

	if (jiffies > timeout) {
		dev_kfree_skb(skb);
		peer->flxnet->stats.rx_errors++;
		if (message_level & NETIF_MSG_RX_ERR)
			pr_err("flxnet_dev: error receiving packet, timeout\n");
		return RX_TIMEOUT;
	}

	if (num_words != length_words) {
		dev_kfree_skb(skb);
		peer->flxnet->stats.rx_errors++;
		if (message_level & NETIF_MSG_RX_ERR)
			pr_err_ratelimited("flxnet_dev: error receiving packet, missing data\n");
		return RX_TRUNCATED;
	}

	if (!is_valid_eop(peer, timeout)) {
		dev_kfree_skb(skb);
		peer->flxnet->stats.rx_errors++;
		if (message_level & NETIF_MSG_RX_ERR)
			pr_err_ratelimited("flxnet_dev: received a packet with invalid trailer\n");
		peer->flxnet->stats.rx_frame_errors += 1;
		return RX_INVALID_EOP;
	}
	
	skb->protocol = eth_type_trans(skb, peer->flxnet);
	skb->ip_summed = CHECKSUM_UNNECESSARY;
	peer->flxnet->stats.rx_packets += 1;	
	update_mac(peer, skb);

	if (message_level & NETIF_MSG_RX_STATUS)
		pr_info("flxnet_dev: received packet (%d bytes, %d words) from peer %p\n",
			skb->len, length_words, peer);
#if LINUX_VERSION_CODE >= KERNEL_VERSION(5,14,0)
	netif_rx(skb);
#else
	netif_rx_ni(skb);
#endif

	if (low_buffer)
		return RX_LOW_BUFFER;
	else
		return RX_SUCCESS;
}


static inline void check_carrier(struct flxnet_peer *peer) {
	bool carrier = 0;
	carrier = status_carrier(peer);

	if (carrier) {
		if (!netif_carrier_ok(peer->flxnet)) {
			if (message_level & NETIF_MSG_IFUP)
				pr_info("flxnet_dev: status carrier changed to %d\n", carrier);
			netif_carrier_on(peer->flxnet);
		}
	}
	else {
		if (netif_carrier_ok(peer->flxnet)) {
			if (message_level & NETIF_MSG_IFDOWN)
				pr_info("flxnet_dev: status carrier changed to %d\n", carrier);
			netif_carrier_off(peer->flxnet);
		}
	}
}


// timer to wake up FIFO polling thread periodically
void recv_timer_function(struct timer_list *timer_list) {
	wake_up_interruptible(&recv_queue);	
	mod_timer(timer_list, jiffies + msecs_to_jiffies(POLL_PERIOD_MS));
}


// returns true if the packet can be received by at least one peer
bool can_transmit_packets(struct flxnet_peer *peer) {

	if (is_magic_correct(peer) && can_accept_block(peer)) {
		return true;
	}
	return false;
}


// iterate over all peers, retrieve all packets under the assumption that CPU can receive them faster than FIFO fills up
void rx_all_packets(struct flxnet_peer *peer) {	
	int length_words, rv;
	t_fifo_word sop;

	if (!is_magic_correct(peer)) {
		pr_err("flxnet_dev: invalid magic in %s, removing peer %p from the network\n",
			__FUNCTION__, peer);
		flxnet_remove_peer(peer);
		return;
	}

	while (is_block_available(peer)) {
		if (message_level & NETIF_MSG_RX_STATUS)
			pr_info("flxnet_dev: peer %p has data block available\n", peer);

		sop = recv_word(peer);
		if ((sop & FLXNET_SOP_MASK) != FLXNET_SOP) {
			if (message_level & NETIF_MSG_RX_ERR)
				pr_err("flxnet_dev: invalid SOP: %#010X\n", sop);
			peer->flxnet->stats.rx_frame_errors += 1;
			continue;
		}

		// found the beginning of a packet
		length_words = sop & FLXNET_LEN_MASK;
		rv = recv_packet(peer, length_words); 				
		if (rv == RX_TIMEOUT || rv == RX_LOW_BUFFER) {
			// receiving this packet took too long, move on to the next peer
			if (message_level & NETIF_MSG_RX_STATUS)
				pr_err("flxnet_dev: low buffer on peer %p, abort reading\n", peer);
			break;
		} else if (rv == RX_NO_MEMORY) {
			pr_err("flxnet_dev: RX thread is out of memory\n");
			return;
		}

	}	

}


// background task for polling the FIFOs and reading out packets
int send_recv_thread(void *data) {
	struct flxnet_peer *peer, *tmp;
	static int carrier_timer = 0;

	DEFINE_WAIT(wait);

	if (message_level & NETIF_MSG_DRV)
		pr_info("flxnet_dev: entered FIFO polling thread\n");

	while (!kthread_should_stop()) {
		mutex_lock(&peer_mutex);
		list_for_each_entry_safe(peer, tmp, &peer_list, peer_entry) {

			if (tx_queue_is_empty(peer)) {
				prepare_to_wait(&recv_queue, &wait, TASK_INTERRUPTIBLE);
				schedule();
				finish_wait(&recv_queue, &wait);
			}
					
			if (!list_empty(&peer_list)) {
				// check carrier every 1000ms 
				if (carrier_timer++ == 1000) {
					check_carrier(peer);
					carrier_timer = 0;
				}
			}

			//if (message_level & NETIF_MSG_INTR)
			// 	pr_info("flxnet: wake up worker thread %s\n", __FUNCTION__);

			tx_all_packets(peer);

			if (kthread_should_stop())
				break;

			if (atomic_read(&peer_count) == 0)
				continue;	

			// restart TX queue if it's stalled
			if (!netif_running(peer->flxnet) && can_transmit_packets(peer)) {
				if (message_level & NETIF_MSG_DRV)
					pr_info("flxnet_dev: wake up net if queue (%s)\n", __FUNCTION__);
				netif_wake_queue(peer->flxnet);
			}
			rx_all_packets(peer);
		}
		mutex_unlock(&peer_mutex);
	}

	if (message_level & NETIF_MSG_IFDOWN)
		pr_info("flxnet_dev: stopping %s\n", __FUNCTION__);
	return 0;
}

int open(struct net_device *dev) {

	mutex_lock(&flxnet_mutex);
	tx_queue_reset(dev); 		// clear TX queue

	// enable TX side
	if (message_level & NETIF_MSG_IFUP)
		pr_info("flxnet_dev: start netif queue\n");
	netif_start_queue(dev);

	mutex_unlock(&flxnet_mutex);
	return 0;
}


int stop(struct net_device *dev) {
	mutex_lock(&flxnet_mutex);
	// disable TX side
	if (message_level & NETIF_MSG_IFDOWN)
		pr_info("flxnet_dev: stop netif queue\n");
	netif_stop_queue(dev);

	tx_queue_reset(dev); 		// clear TX queue
	mutex_unlock(&flxnet_mutex);
	
	return 0;
}


static inline void drop_this_packet(struct flxnet_peer *peer) {
	tx_queue_pop(peer);
	peer->flxnet->stats.tx_dropped += 1;
}


int transmit_packet(struct flxnet_peer *peer, struct tx_packet *packet) {

	int num_words, i;
	t_fifo_word data;

	if (!is_magic_correct(peer)) {
		pr_err("flxnet_dev: invalid magic in %s, removing peer %p from the network,"
			" and dropping the packet\n", __FUNCTION__, peer);
		flxnet_remove_peer(peer);		
		peer->flxnet->stats.tx_dropped += 1;
		return TX_ERROR;
	}

	if (!can_accept_block(peer))
		return TX_BUSY;

	num_words = packet->num_words;

	if (message_level & NETIF_MSG_PKTDATA)
		pr_info("flxnet_dev: transmitting data:\n");
	send_word(peer, FLXNET_SOP | num_words);
	for (i = 0; i < num_words; i++) {
		memcpy(&data, &packet->data[i * BYTES_IN_WORD], BYTES_IN_WORD);
		send_word(peer, data);
		if (message_level & NETIF_MSG_PKTDATA)
			pr_info("%08x\n", data);
	}
	send_eop(peer);

	if (message_level & NETIF_MSG_TX_DONE)
		pr_info("flxnet_dev: sent packet (%d words) to peer %p\n", num_words, peer);

	peer->flxnet->stats.tx_packets += 1;
	peer->flxnet->stats.tx_bytes += num_words * BYTES_IN_WORD;
	return TX_SUCCESS;
}


int send_broadcast(struct tx_packet *packet, struct flxnet_peer *peer) {
	int rv;	
	rv = TX_BUSY;

	if (transmit_packet(peer, packet) != TX_BUSY)			
		rv = TX_SUCCESS;
	
	if (rv == TX_SUCCESS) tx_queue_pop(peer);

	if ((rv == TX_BUSY) && (message_level & NETIF_MSG_TX_ERR)) {
		pr_info("flxnet_dev: no peer has space in FIFO (total %d peers)\n", atomic_read(&peer_count));
	}

	return rv;
}


// send out the latest buffered packet 
int send_packet(struct tx_packet *packet, struct flxnet_peer *peer) {
	int rv;

	packet->attempts += 1;
	if (packet->attempts > MAX_RETRY_ATTEMPTS) {
		drop_this_packet(peer);
		if (message_level & NETIF_MSG_TX_ERR)
			pr_info("flxnet_dev: packet %p is dropped after %d attempts\n",
				packet, packet->attempts);
		return TX_ERROR;
	}

	if (is_multicast_ether_addr(packet->dest_mac))
		return send_broadcast(packet, peer);

	if (memcmp(peer->mac_addr, packet->dest_mac, ETH_ALEN) == 0) {
		// matching peer is found in the table
		rv = transmit_packet(peer, packet);
		if (rv != TX_BUSY) tx_queue_pop(peer);
		return rv;
	}

	// matching peer was not found on the list
	return send_broadcast(packet, peer);
}


static void tx_all_packets(struct flxnet_peer *peer) {
	struct tx_packet *packet;

	while (true) {
		packet = tx_queue_get_next(peer);
		if (packet == NULL) break;
		if (send_packet(packet, peer) == TX_BUSY) break;
	}
}


int hard_start_xmit(struct sk_buff *skb, struct net_device *dev) {

	netif_trans_update(dev);
	if (tx_queue_try_push(skb, dev) == TX_BUSY) {
		if (message_level & NETIF_MSG_TX_ERR)
			pr_info("flxnet_dev: TX packet queue is full\n");
		netif_stop_queue(dev);
		return NETDEV_TX_BUSY;
	}
	
	dev_kfree_skb(skb);
	return NETDEV_TX_OK;
}

#if LINUX_VERSION_CODE >= KERNEL_VERSION(4,18,0)
static void tx_timeout (struct net_device *dev, unsigned int txqueue)
#else
static void tx_timeout (struct net_device *dev)
#endif
{
	dev->stats.tx_errors++;
	dev->stats.tx_dropped++;

	netif_trans_update(dev);
	if (message_level & NETIF_MSG_DRV)
		pr_info("flxnet_dev: wake up netif queue (%s)\n", __FUNCTION__);

	netif_wake_queue(dev);
}

static struct net_device_stats * get_stats(struct net_device *dev) {
	return &dev->stats;
}

static void flx_get_drvinfo(struct net_device *dev, struct ethtool_drvinfo *info) {
	strlcpy(info->driver, DRV_NAME, sizeof(info->driver));
}

static int flx_get_link_ksettings(struct net_device *dev,
				  struct ethtool_link_ksettings *cmd) {
	cmd->base.speed = SPEED_10;
	cmd->base.duplex = DUPLEX_FULL;
	cmd->base.port = PORT_OTHER;
	return 0;
}

static int flx_set_link_ksettings(struct net_device *dev,
				  const struct ethtool_link_ksettings *cmd) {
	return -EINVAL;
}

static u32 flx_get_link(struct net_device *dev) {
	return (u32)(atomic_read(&peer_count) != 0);
}

static u32 flx_get_msglevel(struct net_device *dev) {
	return message_level;
}

static void flx_set_msglevel(struct net_device *dev, u32 v) {
	message_level = v;
}


static const struct ethtool_ops ethtool_ops = {
	.get_drvinfo = flx_get_drvinfo,
	.get_link = flx_get_link,
	.get_msglevel = flx_get_msglevel,
	.set_msglevel = flx_set_msglevel,
	.get_link_ksettings = flx_get_link_ksettings,
	.set_link_ksettings = flx_set_link_ksettings,
};


static const struct net_device_ops flx_netdev_ops = {
	.ndo_open            = open,
	.ndo_stop            = stop,
	.ndo_start_xmit      = hard_start_xmit,
	.ndo_get_stats       = get_stats,
	.ndo_tx_timeout 	 = tx_timeout,
	.ndo_set_mac_address = eth_mac_addr,
	.ndo_validate_addr	 = eth_validate_addr,
};

union zynq_hw_addr {
	char hw_addr[ETH_ALEN];
	struct {
		u32 version;
		u32 idcode;
	};
};

union zynq_hw_addr zynq_data;

#ifdef IS_ROLE_TARGET
static void __iomem *dst;
#endif

// register the network device
int register_network(struct flxnet_peer *peer) {
	int rv;
	message_level = 0;

	// allocate ring buffer for outgoing packets
	peer->tx_queue.packets = kcalloc(tx_queue_size, sizeof(struct tx_packet), GFP_KERNEL);
	if (IS_ERR(peer->tx_queue.packets)) {
		pr_err("flxnet_dev: can't allocate memory for the TX packet queue\n");
		rv = -ENODEV;
		goto err_alloc_tx_queue;
	}

	peer->tx_queue.head = 0;
	peer->tx_queue.tail = 0;

	peer->flxnet = alloc_etherdev(0);
	if (IS_ERR(peer->flxnet)) {
		pr_err("flxnet_dev: can't allocate memory for the network device structure\n");
		rv = -ENODEV;
		goto err_alloc_etherdev;
	}

	strlcpy(peer->flxnet->name, "flxnet%d", IFNAMSIZ);
	peer->flxnet->netdev_ops = &flx_netdev_ops;
	peer->flxnet->ethtool_ops = &ethtool_ops;
	peer->flxnet->watchdog_timeo = msecs_to_jiffies(TX_TIMEOUT_MS);
	peer->flxnet->tx_queue_len = 100;
	peer->flxnet->features = NETIF_F_HW_CSUM;

	#ifdef IS_ROLE_TARGET
	// get versal device_id and parse address
		dst = ioremap(0xF1250000, 4096);
		if (message_level & NETIF_MSG_PROBE)
			pr_info("flxnet_dev: ioremap done\n");

		uint32_t dev_id_h = readl(dst + 0x20);
		uint32_t dev_id_l = readl(dst + 0x24);
		
		if (message_level & NETIF_MSG_PROBE)
			pr_info("flxnet_dev: versal   %08lX %08lX\n", dev_id_h, dev_id_l);

		iounmap(dst);

		zynq_data.idcode = dev_id_h;
		zynq_data.version = dev_id_l;

		zynq_data.hw_addr[0] &= 0xfe;	// clear multicast bit
		zynq_data.hw_addr[0] |= 0x02;	// set local assignment bit (IEEE802)
		eth_hw_addr_set(peer->flxnet, zynq_data.hw_addr);
		if (message_level & NETIF_MSG_PROBE)
			pr_info("flxnet_dev: versal ethernet adress set\n");

	#else 
		eth_random_addr(peer->flxnet->dev_addr);
		if (message_level & NETIF_MSG_PROBE)
			pr_info("flxnet_dev: random ethernet adress set\n");
	#endif
	
	rv = register_netdev(peer->flxnet);
	if (rv) {
		pr_err("flxnet_dev: can't register the network interface\n");
		goto err_register;
	}

	pr_info("flxnet_dev: registered network interface %s with MAC %pM\n", peer->flxnet->name, peer->flxnet->dev_addr);
 
	return 0;

err_register:
	free_netdev(peer->flxnet);

err_alloc_etherdev:
	kfree(peer->tx_queue.packets);

err_alloc_tx_queue:
	return rv;
}


/** @brief
* add a peer device to the network
*/
struct flxnet_peer * flxnet_add_peer(void* __iomem base_address) {
	struct flxnet_peer *peer;
	pr_info("flxnet_dev: flxnet_add_peer %lX\n", (long unsigned int)base_address);

	if (list_empty(&peer_list)) {
		// enable RX side when the first peer is added
		if (message_level & NETIF_MSG_IFUP)
			pr_info("flxnet_dev: initialize RX thread and timer\n");
		recv_task = kthread_create(send_recv_thread, NULL, "flxnet RX thread");
		if (IS_ERR(recv_task)) {
			pr_err("flxnet_dev: failed to create RX thread in %s\n", __FUNCTION__);
			return NULL;
		}
		wake_up_process(recv_task);
		mod_timer(&recv_timer, jiffies + msecs_to_jiffies(POLL_PERIOD_MS));
	}

	mutex_lock(&peer_mutex);
	peer = kzalloc(sizeof(struct flxnet_peer), GFP_KERNEL);
	if (IS_ERR(peer)) {
		pr_err("flxnet_dev: can't allocate memory for struct flxnet_peer\n");
		return NULL;
	}

	register_network(peer);

	peer->regs = base_address;
	

	#ifdef IS_ROLE_HOST
		peer->reg_send = HOST_ADDR_SEND;
		peer->reg_recv = HOST_ADDR_RECV;
		peer->reg_status = HOST_ADDR_STATUS;
	#else
		peer->reg_send = TARGET_ADDR_SEND;
		peer->reg_recv = TARGET_ADDR_RECV;
		peer->reg_status = TARGET_ADDR_STATUS;
	#endif

	set_carrier(peer, true);
	if (message_level & NETIF_MSG_IFUP)
		pr_info("flxnet_dev: status_carrier %d\n", status_carrier(peer));

	list_add(&peer->peer_entry, &peer_list);
	atomic_inc(&peer_count);
	mutex_unlock(&peer_mutex);

	if (message_level & NETIF_MSG_LINK)
		pr_info("flxnet_dev: added peer %p to the network\n", peer);

	return peer;
}

int __init flxnet_dev_init(void) {
	pr_info("flxnet_dev: initializing %s driver\n", DRV_NAME);
	mutex_init(&peer_mutex);
	mutex_init(&flxnet_mutex);
	atomic_set(&peer_count, 0);	
	init_waitqueue_head(&recv_queue);
	timer_setup(&recv_timer, recv_timer_function, 0);
	INIT_LIST_HEAD(&peer_list);
	return 0;
}

void __exit flxnet_dev_exit(void) {	
	pr_info("flxnet_dev: removing %s driver\n", DRV_NAME);

	mutex_destroy(&peer_mutex);
	mutex_destroy(&flxnet_mutex);
}

MODULE_AUTHOR("Elena Zhivun <elena.zhivun@cern.ch>");
MODULE_AUTHOR("Laura Rientsma <l.rientsma@nikhef.nl>");
MODULE_DESCRIPTION("Driver on both host PC and Versal card for establishing virtual network over PCIe");

module_init(flxnet_dev_init);
module_exit(flxnet_dev_exit);

EXPORT_SYMBOL(flxnet_add_peer); 
EXPORT_SYMBOL(flxnet_remove_peer);

MODULE_LICENSE("GPL");
