/*

	This is the network driver for host side
	to create a network interface over PCIe

	When module is loaded:
	1. Finds a FLX-182 PCIe card (vendor=0x10dc, device=0x0427)
	2. Enables the PCIe device, reserve and map the BAR if BAR #3 is available
	3. Register the peer with flxnet network device
	4. Repeats untill all the PCIe devices available are configured with a network interface
	5. If BAR #3 is not available, does nothing

*/

#include <linux/kernel.h>
#include <linux/moduleparam.h>
#include <linux/stat.h>
#include <linux/mm.h>
#include <linux/pci.h>
#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/vmalloc.h>
#include <linux/module.h>

#include "flxnet/flxnet.h"

extern struct flxnet_peer * flxnet_add_peer(void* __iomem base_address);
extern void flxnet_remove_peer(struct flxnet_peer *peer);

#define DRV_MODULE_NAME	"flxnet_pcie"

#define FLX_PCIE_MAGIC	0xDED133D0UL

struct pcie_flx_serial_device {
	struct list_head fdev_entry;
	unsigned long magic;		// structure ID for sanity checks 
	struct pci_dev *pdev;		// pci device struct from probe()
	void __iomem *bar;			// addresses for the relevant BAR
	int bar_idx;
	unsigned int flags;

	struct flxnet_peer * peer;
};

// which bar number the AXI serial device is mapped to
static int bar_idx = 3;

// which offset the AXI serial device is found at
static uint dev_base_addr = 0x0;

// list of FELIX devices
struct list_head fdev_list;	

static int configure_pcie_device(struct pci_dev *pdev);
static void deconfigure_device(struct pcie_flx_serial_device *fdev);
static int map_single_bar(struct pcie_flx_serial_device *fdev, int idx);

static int flxnet_pcie_init(void) {
	int rv = -ENODEV;
	struct pci_dev *pdev; // previous device
	pdev = NULL; 		  // a new search for PCI devices is initiated by passing NULL as the pdev argument

	INIT_LIST_HEAD(&fdev_list);

	pr_info("flxnet_pcie: loading %s module\n", DRV_MODULE_NAME);	
	 
	// if a PCI device is found, a pointer to its device structure is returned
	pdev = pci_get_device(0x10dc, 0x0427, pdev);

	if (pdev == NULL) {
		pr_info("flxnet_pcie: no pcie devices available for flxnet\n");
		return 0;
	}

	// pdev returns NULL when all the PCI devices are found and stops searching
	while (pdev != NULL) {
		pr_info("flxnet_pcie: found %s \n", pci_name(pdev));
		rv = configure_pcie_device(pdev);

		if (rv < 0) {			
			pr_warn("flxnet_pcie: failed to configure %s\n", pci_name(pdev));
		}

		pdev = pci_get_device(0x10dc, 0x0427, pdev);

		if (pdev == NULL) {
			pr_info("flxnet_pcie: no more pcie devices found\n");
		}
	}
	return rv;	
}


// deconfigure pcie devices if necessary, remove module
static void flxnet_pcie_exit(void) {

	struct pcie_flx_serial_device *fdev, *tmp;

	pr_info("flxnet_pcie: removing %s module\n", DRV_MODULE_NAME);

	if (!list_empty(&fdev_list)) {
		pr_info("flxnet_pcie: list not empty - deconfiguring PCIe devices\n");
		list_for_each_entry_safe(fdev, tmp, &fdev_list, fdev_entry) {
			flxnet_remove_peer(fdev->peer);
			deconfigure_device(fdev);
		}
	}
	else {
		pr_info("flxnet_pcie: list is empty - nothing to deconfigure\n");
	}
}


// configure a felix serial device
static int configure_pcie_device(struct pci_dev *pdev) {

	int rv;
	struct pcie_flx_serial_device *fdev;

	if (!pdev) {
		pr_err("flxnet_pcie: invalid PCIe device structure\n");
		return -EFAULT;
	}

	pr_info("flxnet_pcie: initializing the PCIe device\n");

	fdev = kzalloc(sizeof(struct pcie_flx_serial_device), GFP_KERNEL);
	if (!fdev)
		return -ENOMEM;

	fdev->magic = FLX_PCIE_MAGIC;
	fdev->pdev = pdev;
	fdev->bar_idx = bar_idx;

	pr_info("flxnet_pcie: enabling PCIe device\n");
	rv = pci_enable_device(pdev);
	if (rv) {
		pr_err("flxnet_pcie: pci_enable_device() failed, %d\n", rv);
		goto err_enable;
	}
	pci_dev_get(pdev);

	// reserve the selected bar for this driver
	pr_info("flxnet_pcie: reserving BAR memory\n");
	rv = pci_request_region(pdev, bar_idx, DRV_MODULE_NAME);
	if (rv) {
		pr_err("flxnet_pcie: pci_request_region() failed (%d)\n", rv);
		goto err_region;
	}		


	pr_info("flxnet_pcie: mapping BAR memory\n");
	rv = map_single_bar(fdev, bar_idx);
	if (rv < 0) {
		goto warn_nobar;
	}
	else if (rv < MIN_MMAP_SIZE) {
		pr_err("flxnet_pcie: mapped region is too small (%d bytes < %d)\n",
			rv, MIN_MMAP_SIZE);
		goto err_peer;
	}

	// add this device to the network
	pr_info("flxnet_pcie: inserting peer into flxnet\n");
	fdev->peer = flxnet_add_peer(fdev->bar + dev_base_addr);

	if (IS_ERR(fdev->peer)) {
		pr_err("flxnet_pcie: error inserting peer into flxnet\n");
		goto err_peer;
	}


	// device structure
	list_add(&fdev->fdev_entry, &fdev_list);
	pr_info("flxnet_pcie: added peer to list flx_devices\n");
	return 0;

warn_nobar:
	pr_warn("flxnet_pcie: no network registered, disabling PCI device\n");
	pci_disable_device(pdev);
	pci_release_region(pdev, bar_idx);
	return 0;

err_peer:
	pr_info("flxnet_pcie: unmapping BAR memory\n");
	pci_iounmap(pdev, fdev->bar);

err_enable:
	pr_err("flxnet_pcie: pdev 0x%p, err %d\n", pdev, rv);
	kfree(fdev);
	return rv;

err_region:
	pci_disable_device(pdev);
	goto err_enable;
}


// unregister and deconfigure a felix serial device
static void deconfigure_device(struct pcie_flx_serial_device *fdev) {
	struct pci_dev * pdev;

	pr_info("flxnet_pcie: deconfiguring device %p\n", fdev);

	if (!fdev) {
		pr_err("flxnet_pcie: felix_flx_serial_device ptr is NULL\n");
		return;
	}

	if (fdev->magic != FLX_PCIE_MAGIC) {
		pr_err("flxnet_pcie: incorrect MAGIC value in deconfigure_device()\n");
		return;
	}

	pdev = fdev->pdev;

	if (!pdev) {
		pr_err("flxnet_pcie: pci_dev ptr is NULL\n");
	}

	if (fdev->bar) {
		pci_iounmap(pdev, fdev->bar);		
	} else {
		pr_warn("flxnet_pcie: BAR memory is not mapped\n");
	}

	pci_dev_put(pdev);
	pci_disable_device(pdev);
	pci_release_region(pdev, bar_idx);
	list_del(&fdev->fdev_entry);
	kfree(fdev);
}


// map BAR containing the serial communication registers
static int map_single_bar(struct pcie_flx_serial_device *fdev, int idx) {

	struct pci_dev *pdev = fdev->pdev;
	resource_size_t bar_start = pci_resource_start(pdev, idx);
	resource_size_t bar_len = pci_resource_len(pdev, idx);
	resource_size_t map_len = bar_len;

	fdev->bar = NULL;

	// do not map BARs with length 0. Note that start MAY be 0!
	if (!bar_len) {
		pr_info("flxnet_pcie: BAR #%d is not present\n", idx);
		return -1;
	}

	// BAR size exceeds maximum desired mapping?
	if (bar_len > INT_MAX) {
		pr_info("flxnet_pcie: limit BAR %d mapping from %llu to %d bytes\n", idx,
			(u64)bar_len, INT_MAX);
		map_len = (resource_size_t)INT_MAX;
	}

	// map the full device memory or IO region into kernel virtual address space
	fdev->bar = pci_iomap(pdev, idx, map_len);

	if (!fdev->bar) {
		pr_info("flxnet_pcie: could not map BAR %d\n", idx);
		return -1;
	}

	pr_info("flxnet_pcie: BAR%d at 0x%llx mapped at 0x%p, length=%llu(/%llu)\n", idx,
		(u64)bar_start, fdev->bar, (u64)map_len, (u64)bar_len);

	return (int)map_len;
}


module_init(flxnet_pcie_init);
module_exit(flxnet_pcie_exit);
module_param(bar_idx, int, S_IRUSR);
MODULE_PARM_DESC(bar_idx, "BAR of the FIFO AXI device");
module_param(dev_base_addr, uint, S_IRUSR);
MODULE_PARM_DESC(dev_base_addr, "Base offset of the FIFO AXI device");

MODULE_AUTHOR("Elena Zhivun <elena.zhivun@cern.ch>");
MODULE_AUTHOR("Laura Rientsma <l.rientsma@nikhef.nl>");
MODULE_DESCRIPTION("Driver on host PC for establishing virtual network over PCIe, registers with flxnet_dev");
MODULE_LICENSE("GPL");
